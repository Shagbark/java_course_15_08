package org.levelup.lesson2;

import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    // Программа генерирует случайное число
    // Мы вводим число с клавиатуры
    // Сравниваем числа

    public static void main(String[] args) {

        // int a;
        Random randomizer = new Random();
        int secretNumber = randomizer.nextInt(4); //+ 100; // [0, 4) [100, 104)

        // [0, 4) + 4 = [4, 8)
        // [-10, 11) -> nextInt(21) - 10;


        // nextInt(5) - 10 -> [-10, -6]
        // nextInt(5) -> [0, 4] -> 0, 1, 2, 3, 4 - 10 -> -10, -9, -8, -7, -6 -> [-10, -6]

        Scanner consoleReader = new Scanner(System.in);
        System.out.println("Введите число");
        int userNumber = consoleReader.nextInt();

        //
        if (secretNumber == userNumber) {
            System.out.println("Вы угадали!");
            System.out.println("Секретное число: " + secretNumber);
        } else {
            System.out.println("Вы не угадали!");
            System.out.println("Секретное число: " + secretNumber);
        }

        System.out.println(new Date().getTime());

    }

}
