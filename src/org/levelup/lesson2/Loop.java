package org.levelup.lesson2;

import java.math.BigDecimal;

public class Loop {

    public static void main(String[] args) {
        double number = 0;
        for (; number < 1; number += 0.2) { // number = number + 0.2
            System.out.println(number * number);
        }

        // for (; условие; ) -> while (условие)

        double first = 0.5;
        double second = 0.3 + 0.1;  // 0.40000000000000000000000000000000000000000000000001
                                    // 0.3999999999999999999999999999999999999999999999999

        int result = Double.compare(first, second);
        // first > second -> >= 1 (> 0) 1
        // first == second -> 0
        // first < second -> <= -1 (< 0) -1
        System.out.println(result);
//        boolean result = first == second;
//        System.out.println(result);


        // while (true) {}

    }

}
