package org.levelup.lesson2;

import java.math.BigDecimal;

public class PrimitiveTypes {

    // sout
    // psvm
    public static void main(String[] args) {
        int a = 10;
        byte b = 20;

        // cast
        long l = a; // неявное
        byte d = (byte) a; // явное

        // byte c = (byte) a + b;
        byte c = (byte) (a + b);

        byte s = (byte) 128; // -> -128
                        // 129 -> -127
                        // 130 -> -126

        // 3242 - int
        // 43432L (l)- long
        // 6.54D (d) - double
        // 534.3F (f) - float
        // byte q = c + 5.54f;
        // float floatValue = 42.43F;

        // BigDecimal


        byte value = 14;
        int intValue = 58;
        // value = (byte) (value + intValue);
        value += intValue;

        //
        boolean is = value == intValue || isAdmin();

    }

    private static boolean isAdmin() {
        /// code here
        return false;
    }

}
