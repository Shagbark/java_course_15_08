package org.levelup.lesson2;

public class Array {

    public static void main(String[] args) {
        int a1, a2, a3;

        int[] array = new int[10];
        // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        array[0] = 45;
        // [45, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        array[1] = 65;
        array[4] = 54;
        // [45, 65, 0, 0, 54, 0, 0, 0, 0, 0]
        array[9] = 98;
        // [45, 65, 0, 0, 54, 0, 0, 0, 0, 98]
        // array[10] = 535;

        // System.out.println(array[9]);
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }


        // int[][] doubleArray = new int[13][15];

    }


}
