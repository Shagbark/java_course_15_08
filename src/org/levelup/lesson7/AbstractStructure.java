package org.levelup.lesson7;

public abstract class AbstractStructure implements Structure {

    protected int size;

    @Override
    public void addArray(int[] value) {
        for (int i = 0; i < value.length; i++) {
            addLast(value[i]);
            size++;
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

}
