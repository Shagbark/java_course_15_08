package org.levelup.lesson7;

public interface Structure {

    // public static final ...
    // public abstract ...

    void addArray(int[] array);

    void addLast(int value);

    int getSize();

    boolean isEmpty();

}
