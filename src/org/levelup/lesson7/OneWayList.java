package org.levelup.lesson7;

import java.io.Serializable;

// Элемент список - совокупность значения и ссылки на след элемент
public class OneWayList extends AbstractStructure {

    private Element head;

    @Override
    public void addLast(int value) {
        Element element = new Element(value);
        if (head == null) {
            head = element;
        } else {
            Element current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(element);
        }
    }

    public void addFirst(int value) {
        Element element = new Element(value);
        if (head == null) {
            head = element;
        } else {
            element.setNext(head);
            head = element;
        }
        size++;
    }

}
