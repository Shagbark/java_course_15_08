package org.levelup.lesson7;

import org.levelup.lesson6.DynamicArray;

public class Main {

    public static void main(String[] args) {
        // AbstractStructure abstractStructure = new AbstractStructure();
        AbstractStructure structure = new DynamicArray(5);
        int[] array = new int[] {1, 5, 3, 9}; // -> [1, 5, 3, 9]
        structure.addArray(array);
        System.out.println();
    }

}
