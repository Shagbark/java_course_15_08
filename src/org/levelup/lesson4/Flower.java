package org.levelup.lesson4;

@SuppressWarnings("ALL")
public final class Flower {

    // Модификатор доступа
    // private
    // default-package (private-package)
    // protected
    // public
    //  flower.setName("Цветок");
    //  System.out.println(flower.getName());
    private String name;
    String color;  // default-package (private-package)
    public double weight;

    public final void setName(String name) {
        // Flower.this.getName();
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
