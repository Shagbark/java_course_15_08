package org.levelup.lesson4.inheritance;

public class InheritanceExample {

    public static void main(String[] args) {
        Car car = new Car();
        car.wheelCount = 4;
        car.go();

        Car c = new Car();
        c.wheelCount = 3;
        System.out.println("C: " + c.wheelCount);
        System.out.println("Car: " + car.wheelCount);

        Bus bus = new Bus();
        bus.wheelCount = 6;
        bus.setSitsAmount(50);

        Transport transport = new Transport();
        transport.go();

        Ship ship = new Ship();
        ship.go();

        BigShip bigShip = new BigShip();
        bigShip.go();

        // DRY - don't repeat yourself
        Transport[] transports = new Transport[3];
        transports[0] = bus;
        transports[1] = transport;
        transports[2] = bigShip;
        printAllTransport(transports);
    }

    // Car[], Bus[], SportCar[], Ship[], BigShip[]
    static void printAllTransport(Transport[] transports) {
        for (int i = 0; i < transports.length; i++) {
            transports[i].go();
            // System.out.println(transports[i].wheelCount);
        }
    }

//    static void printAllTransport(Car[] transports) {
//        for (int i = 0; i < transports.length; i++) {
//            System.out.println(transports[i].wheelCount);
//        }
//    }

}
