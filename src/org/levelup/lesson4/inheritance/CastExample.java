package org.levelup.lesson4.inheritance;

import java.util.Objects;

// Полиморфизм: один интерфейс, множество реализаций
@SuppressWarnings("ALL")
public class CastExample {

    public static void main(String[] args) {

        Car car = new Car();
        car.wheelCount = 3;

        Transport transport = car;
        Object obj1 = car;
        Object obj2 = transport;

        Transport tr = new Transport();
        Object obj3 = tr;

        Car c = (Car) obj2;
        System.out.println(c.wheelCount);

        Transport t = new BigShip();
        t.go();
        // Car cr = (Car) tr;


    }

}
