package org.levelup.lesson4.inheritance;

public class Bus extends Transport {

    private int sitsAmount;

    public int getSitsAmount() {
        return sitsAmount;
    }

    public void setSitsAmount(int sitsAmount) {
        this.sitsAmount = sitsAmount;
    }

}
