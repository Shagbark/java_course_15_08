package org.levelup.lesson4.inheritance;

public class Ship extends Transport {

    @Override
    public void go() {
        // super.go();
        System.out.println("Корабли ходят, а не плавают!");
    }

}
