package org.levelup.lesson4;

public class FlowerApp {

    public static void main(String[] args) {
        Flower flower = new Flower();
        // flower.name = "";
        flower.setName("Цветок");
        System.out.println(flower.getName());

        Flower anotherFlower = new Flower();
        anotherFlower.setName("");
    }

}
