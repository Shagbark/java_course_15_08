package org.levelup.lesson4.hw;

public class Tea {

    String color;
    String kind;

    public Tea(String color, String kind) {
        this.color = color;
        this.kind = kind;
    }

    public Tea() {
    }

    public String getColor() {
        return color;
    }
}
