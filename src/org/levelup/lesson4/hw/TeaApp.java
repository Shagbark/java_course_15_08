package org.levelup.lesson4.hw;

import java.util.Arrays;
import java.util.Objects;

public class TeaApp {

    public static void main(String[] args) {
        Tea[] teas = new Tea[10];
        teas[0] = new Tea();
        teas[0].color = "Black";
        teas[0].kind = "Kind";
        teas[1] = new Tea("Green", "");

        // [black, green, null, null, null ...]
        for (int i = 0; i < teas.length; i++) {
            if (teas[i] != null) {
                System.out.println(teas[i].color);
            }
        }

//        Arrays.stream(teas)
//            .filter(Objects::nonNull)
//            .map(Tea::getColor)
//            .forEach(System.out::println);

        // black
        // green
        // NPE - null pointer exception
    }

}
