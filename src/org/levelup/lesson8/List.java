package org.levelup.lesson8;

// Inner class - внутренний класс
//      Inner class         Nested class
//      внутренний          вложенный

// List list = new List();
// org.levelup.lesson8.List.Element - полное имя внутреннего класса
// List.Element element = list.new Element();
// List.Element el = new List().new Element();

import java.util.Iterator;

// abstract RateLimiter -> DefaultRateLimiter()
public class List<TYPE> implements Iterable<TYPE> { // Внешний класс

    private Element head;
    private Element tail;

    private int size;

    // getIterator()
    @Override
    public Iterator<TYPE> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<TYPE> {

        private Element curr = head;

        // int sum(a, b) { return a + b; }
        @Override
        public boolean hasNext() {
            // boolean res;
            // if (curr != null)
            //      res = true;
            // else
            //      res = false;
            // return result;

            // boolean result = curr != null;
            // return result;
            return curr != null;
        }

        @Override
        public TYPE next() {
            // Получить текущее значение
            // Передвинуть curr на следующий элемент
            TYPE result = curr.value;
            curr = curr.next;
            return result;
        }
    }

    private class Element {
        Element next;
        TYPE value;
        Element(TYPE value) {
            this.value = value;
        }
    }

    public void add(TYPE value) {
        Element element = new Element(value);
        if (head == null) {
            head = element;
            tail = element;
        } else {
            tail.next = element;
            tail = element;
        }
        size++;
    }

    public TYPE get(int index) {
        if (index < 0 || index >= size) {
            // Заменяет return
            // IndexOutOfBoundException
            // ArrayIndexOutOfBoundException
            throw new IndexOutOfBoundsException();
        }
        Element curr = head;
        // for (int currIndex = 0; currIndex != index; currIndex++) {
        //      curr = curr.next;
        // }
        int currIndex = 0;
        while (currIndex != index) {
            curr = curr.next;
            currIndex++;
        }

        return curr.value;
    }

}
