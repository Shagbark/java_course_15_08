package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class RemoveElement {

    static class Notebook {
        double cpu;
        String model;

        public Notebook(double cpu, String model) {
            this.cpu = cpu;
            this.model = model;
        }

        @Override
        public String toString() {
            return "Notebook{" +
                    "cpu=" + cpu +
                    ", model='" + model + '\'' +
                    '}';
        }
    }

    public static void main(String[] args) {
        List<Notebook> notebooks = new ArrayList<>();
        notebooks.add(new Notebook(1.4, "HP"));
        notebooks.add(new Notebook(2.4, "Lenovo"));
        notebooks.add(new Notebook(2.1, "Lenovo"));
        notebooks.add(new Notebook(1.8, "HP"));

//        for (Notebook notebook : notebooks) {
//            if (notebook.cpu < 2) {
//                notebooks.remove(notebook);
//            }
//        }

        Iterator<Notebook> iterator = notebooks.iterator();
        while (iterator.hasNext()) {
            Notebook notebook = iterator.next();
            if (notebook.cpu < 2) {
                iterator.remove();
            }
        }

        System.out.println(notebooks.toString());

    }

}
