package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.Collection;

public class Application {

    //
    public static void main(String[] args) {
//        List<String> list = new List<>();
//        list.add("");
//        list.add("");
//        list.add("");
//
//        List<Integer> integerList = new List<>();
//        integerList.add(15);
//
//        List<Object> objectList = new List<>();
//        objectList.add("");
//        objectList.add(10);
//
//        List rawList = new List();
//        rawList.add(10);
        List<String> products = new List<>();
        products.add("Помидор");
        products.add("Огурец");
        products.add("Салат");

        // System.out.println(products.get(0));
        for (String p : products) {
            System.out.println(p);
        }

        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(10);
        integers.add(11);
        integers.add(12);
        integers.add(14);
        integers.add(95);
        // foreach
        // for (<generic type> var_name : collection)
        // value = 10
        // value = 11
        // value = 12
        // value = 14
        for (Integer value : integers) {
            System.out.println(value);
        }
        // for (int i = 0; i < arr.length; i++) {
        //      sout(arr[i]);
        // }

        // arr[10]
        // for (int v : arr) {
        //      sout(v);
        // }
    }

}
