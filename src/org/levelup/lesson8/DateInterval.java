package org.levelup.lesson8;

// DateInterval.Date
public class DateInterval {

    public class Date {
        private String date;
        private String time;

        public Date(String date, String time) {
            this.date = date;
            this.time = time;
        }
    }

    public Date getStart() {
        return new Date("", "");
    }

}
