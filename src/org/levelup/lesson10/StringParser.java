package org.levelup.lesson10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

// разбивать/разбирать
public class StringParser {

    public Collection<String> parseLine(String line) {
        // replace
        String[] words = line.replace("!", "")
                .replace("?", "")
                .replace(".", "")
                .replace(",", "")
                .replace(":", "")
                .replace("\"", "")
                .replace("\'", "")
                .replace("(", "")
                .replace(")", "")
                .toLowerCase()
                .split(" ");
        // [] -> Collection?

        // 1 variant
        // Collection<String> wordCollection = new ArrayList<>();
        // for (String word : words) { // for (int i = 0; i < words.length; i++)
        //    wordCollection.add(word);
        // }
        // return wordCollection;

        // 2 variant
        // varargs
        // n(String... strings)
        //        m(new String[0]);
        //        m(new String[] { "" });
        //
        //        String[] s = new String[2];
        //        s[0] = "";
        //        s[1] = "";
        //        m(s);
        return new ArrayList<>(Arrays.asList(words));
        // 3 variant
        // return Arrays.stream(words).collect(Collectors.toList())
        // 3.1 variant parallel
        // return Arrays.stream(words).parallel().collect(Collectors.toList());
    }

    // m(Object... integers, String s);
    // m(String s, Integers... integers)

//    void m(String... strings) {
//        // strings -> String[]
//        // [0]
//        // [1]
//        // [2]
//    }

}
