package org.levelup.lesson10;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;

public class WordCountComparator implements
        Comparator<Map.Entry<String, Integer>> {

    @Override
    public int compare(Map.Entry<String, Integer> o1,
                       Map.Entry<String, Integer> o2) {
        // 0 -> o1 == o2 (o1.equals(o2))
        // >0 -> o1 > o2
        // <0 -> o1 < o2

        // Entry -> пара ключ-значение
//        if (o1.getValue() > o2.getValue()) {
//            return 1;
//        } else if (o1.getValue() == o2.getValue()) {
//            return 0;
//        } else {
//            return -1;
//        }
        return o2.getValue() - o1.getValue();
        // return -(o1.getValue() - o2.getValue());
    }

}
