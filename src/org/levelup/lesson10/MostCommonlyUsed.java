package org.levelup.lesson10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class MostCommonlyUsed {

    public static void main(String[] args) {
        // 1. Прочитать файл
        Reader reader = new Reader("file.txt");
        Collection<String> lines = reader.readAllLines();

        // 2. Разбить строку на слова и убрать все знаки препинания
        // the & the"
        // toLowerCase()
        Map<String, Integer> wordMapping = new HashMap<>();

        StringParser parser = new StringParser();
        for (String line : lines) {
            Collection<String> words = parser.parseLine(line);
            for (String word : words) {
                Integer count = wordMapping.get(word);
                if (count == null) {
                    wordMapping.put(word, 1);
                } else {
                    // count++;
                    // Integer -> int
                    // count = count + 1
                    // int -> Integer

                    // wordMapping.put(word, count (a));
                    // count++ (b);
                    // a != b;

                    // Map<String, Collection<Integer>> map;
                    // Collection<Integer> i = map.get("");
                    // i.add(20);
                    // map.put("", i); можно не выполнять (не писать)

                    // count++;
                    // wordMapping.put(word, count);
                    wordMapping.put(word, ++count);
                }
            }

        }

        // 3. Отсортировать
        List<Map.Entry<String, Integer>> elements =
                new ArrayList<>(wordMapping.entrySet());
        // Comparable
        // Comparator

        // Анонимный внутрениий класс
        // Collections.sort(elements, new WordCountComparator());
        // MostCommonlyUser$1.class
        Collections.sort(elements, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });

        // Collections.sort(elements, (first, second) -> second.getValue() - first.getValue());


        for (int i = 0; i < 10; i++) {
            Map.Entry<String, Integer> el = elements.get(i);
            System.out.println(el.getKey() + " " + el.getValue());
        }

    }

}
