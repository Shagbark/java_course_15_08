package org.levelup.lesson10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class Reader {

    private File file;      // Файл, который будет считываться

    public Reader(String filename) {
        // Создали объект File
        this.file = new File(filename);
    }

    public Collection<String> readAllLines() {
        Collection<String> lines = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            String line;
            // readLine() -> "\n"
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }

        } catch (IOException exc) {
            System.out.println("Ошибка чтения файла...");
            // throw new RuntimeException(exc);
        }

        return lines;
    }

}
