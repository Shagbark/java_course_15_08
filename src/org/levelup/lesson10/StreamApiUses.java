package org.levelup.lesson10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamApiUses {

    public static void main(String[] args) throws IOException {
        StringParser parser = new StringParser();
        Files.lines(Paths.get("file.txt"))
//                .map(line -> parser.parseLine(line))
                .map(parser::parseLine)
                .flatMap(Collection::stream)
                .filter(word -> word != null && !word.trim().isEmpty())
                // .collect(Collectors.groupingBy(word -> word)) -> Map<String, List<String>>
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Comparator.comparingLong(Map.Entry<String, Long>::getValue).reversed())
                .limit(10)
                .forEach(entry -> System.out.println(entry.getKey() + " " + entry.getValue()));

    }

}
