package org.levelup.lesson6;

import org.levelup.lesson7.AbstractStructure;

// Массив, который изменяет свой размер в зависимости от количества элементов
public class DynamicArray extends AbstractStructure {

    // [1, 4, 6, 8, 0, 0], size = 4
    // [1, 4, 6, 8, 0, 0], size = 3
    // int result = array[size - 1]
    // array[size - 1] = 0
    // size--;
    // [1, 4, 6, 0, 0, 0], size = 3

    // push
    // [1, 4, 6, 5, 0, 0], size = 4
    private int[] array;

    public DynamicArray(int capacity) {
        this.array = new int[capacity];
    }

    // Добавление в конец
    @Override
    public void addLast(int value) {
        if (getSize() == array.length) {
            ensureCapacity();
        }
        array[size] = value;
//        array[size] = value;
//        size++;
    }

    public void addFirst(int value) {
        if (size == array.length) {
            ensureCapacity();
        }
        shiftRight();
        // [1, 1, 2, 4]
        array[0] = value;
        // [5, 1, 2, 4]
        size++;
    }

    // [1, 2, 4, 0]
    // [1, 1, 2, 4]
    // [1, 2, 4, 0]
    // i = 3; array[3] = array[2], [1, 2, 4, 4]
    // i = 2; array[2] = array[1], [1, 2, 2, 4]
    // i = 1; array[1] = array[0], [1, 1, 2, 4]
    private void shiftRight() {
        for (int i = size; i > 0; i--) {
            array[i] = array[i - 1];
        }
    }

    // checkAndEnsureCapacity();
    private void ensureCapacity() {
        int[] oldArray = array;
        array = new int[(int)(array.length * 1.5)];
        System.arraycopy(oldArray, 0, array, 0, oldArray.length);
    }

    // Получение значения по индексу

    // toString()
    @Override
    public String toString() {
        if (size == 0) {
            return "[]";
        }

        // "[3, 4, 5, 5]"
        String result = "[";
        for (int i = 0; i < size; i++) {
            result += array[i] + ", ";
        }

        // "[3, 4, 5, 5, "
        // substring(begin)
        // substring(begin, end)
        result = result.substring(0, result.length() - 2);
        return result + "]";
        // return result.substring(0, result.length() - 2) + "]";
    }

}
