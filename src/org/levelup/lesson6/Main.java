package org.levelup.lesson6;

public class Main {

    public static void main(String[] args) {
        // int[] array = new int[10];
        // [0, 0, 0, .. 0]
        // is empty
        // [43, 0, 0, 0, 0 .. ]

        DynamicArray dynamicArray = new DynamicArray(10);

        // array[0] = 43; [43]
        // array[1] = 41; [43, 41]
        // array[2] = 64; [43, 41, 64]
        // ...
        // array[9] = 53; [43, 41, 64, ... , 53]
        dynamicArray.addLast(43);
        dynamicArray.addLast(53);
        dynamicArray.addLast(85);
        dynamicArray.addLast(72);
        dynamicArray.addLast(105);
        dynamicArray.addLast(87);
        dynamicArray.addLast(6);
        dynamicArray.addLast(75);
        dynamicArray.addLast(8);
        dynamicArray.addLast(45);
        dynamicArray.addLast(12);
        dynamicArray.addLast(5);

        // org.levelup.lesson6.DynamicArray@a35b52c3c
        System.out.println(dynamicArray.toString());

    }

}
