package org.levelup.lesson5;

public class App {

    public static void main(String[] args) {
        Product tomato  = new Product("Томат", 45.32, 235.43, 40);
        Product tomato2 = new Product("Томат", 45.32, 235.43, 40);
        Product apple   = new Product("Яблоко", 4.6, 53.3, 56);

        // tomato == tomato2 -> false
        boolean isEqual = tomato.equals(tomato2); // -> false

        boolean result = tomato.equals("tomato");


        boolean equalsResult = tomato.equals(tomato2);
        boolean hashCodeResult = tomato.hashCode() == tomato2.hashCode();
        System.out.println(hashCodeResult);
        // tomato.equals(null) -> false

        for (int i = 0; i < 1000000000; i++) {
            if (tomato.hashCode() == tomato2.hashCode()) {
                if (tomato.equals(tomato2)) {
                    // Объекты одинаковы
                }
            }
        }

    }

}
