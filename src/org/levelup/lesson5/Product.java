package org.levelup.lesson5;

import java.util.Objects;

public class Product implements Comparable<Product> {

    private final String name;
    private double weight;
    private double price;
    private int energy;

    public Product(String name, double weight, double price, int energy) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.energy = energy;
    }


    // обратная польская нотация
    // 100 + 21 / 3
    //  / 21 3 -> 7
    // /
    // 21
    // 3

    // [][]()(()()((()))))[]
    // )(][][][]][

    // Что нужно делать в equals
    // 1. Проверить, что переданный объект - та же самая ссылка
    //      if (this == obj) return true;
    // 2. Проверить, что объект (obj) не null
    // 3. Проверить совпадение классов у объектов
    //          this.getClass() == obj.getClass()
    //          obj instanceof Product
    // 4. Привести obj к типу Product (того же типа что и this)
    // 5. Сравниваем поля (и проверяем, что поля не null)

    // tomato -> this
    // tomato2 -> obj (other)
    @Override
    public boolean equals(Object obj) {
        // obj instanceof Type(Class)
        // obj instanceof Product
        if (this == obj) return true;

        // if (obj == null || getClass() != obj.getClass()) return false;
        // null instanceof Object -> false
        if (!(obj instanceof Product)) return false;
        final Product other = (Product) obj; // ClassCastException

        // return (name != null && name.equals(other.name)) && energy == other.energy;
        // null == null -> true
        // Objects
        return Objects.equals(name, other.name) && energy == other.energy;
    }

    @Override
    public int hashCode() {
        // return Objects.hash(name, energy);
        int result = 31; //

        result += 31 * result + energy;
        result += 31 * result + name.hashCode();

        return result;
    }

    @Override
    public int compareTo(Product o) {
        return 0;
    }
}
