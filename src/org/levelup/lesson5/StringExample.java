package org.levelup.lesson5;

@SuppressWarnings("ALL")
public class StringExample {

    //VisualVM
    public static void main(String[] args) {
        String s1 = "s1";
        String s2 = "s1";
        String s3 = new String("s1");

        // 1. true, false, true
        // 2. true, true, true
        // 3. false, false, true
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s1.equals(s3));
    }
}
