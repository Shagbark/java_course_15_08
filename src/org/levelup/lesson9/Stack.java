package org.levelup.lesson9;

public class Stack {

    // EmptyStackException
    // StackOverflowException

    private int[] data;
    private int size;

    public Stack(int capacity) {
        this.data = new int[capacity];
    }

    public void push(int value) throws StackOverflowException {
        if (size == data.length) {
            // throw exception
            throw new StackOverflowException();
        }
        data[size++] = value;
    }

    public int pop() {
        if (size == 0) {
            // throw exception
            throw new EmptyStackException();
        }
        return data[--size];
    }

}
