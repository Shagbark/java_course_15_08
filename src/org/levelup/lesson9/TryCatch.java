package org.levelup.lesson9;

@SuppressWarnings("ALL")
public class TryCatch {

    public static void main(String[] args) {
        int result = calculate();
        System.out.println(result);
    }

    static int calculate() {
        try {
            // throw new Exception();
            // code ..
            // System.exit(0);
            return 1;
        } catch (Exception exc) {
            return 2;
        } finally {
            //return 3;
            System.out.println("Test");
        }
    }

}
