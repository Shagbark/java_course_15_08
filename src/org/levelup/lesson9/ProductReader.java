package org.levelup.lesson9;

import java.io.*;

public class ProductReader {

    public static void main(String[] args) {
        File products = new File("products.txt");

        File src = new File("/src"); //
        File products2 = new File("products2.txt"); // products2.exists() -> false

        // src.isDirectory() -> true

        if (products.exists()) {

            // Reader, InputStream
            // FileReader, FileInputStream
            // InputStreamReader
            // BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

//            BufferedReader reader = null;
//            try {
//                reader = new BufferedReader(new FileReader(products));
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    System.out.println(line);
//                }
//            // catch (IOException io) {}
//            // catch (Throwable t) {}
//            // catch (IOException | NullPointerException exc) {}
//            } catch (FileNotFoundException fnf) {
//                System.out.println("Файл не найден...");
//            } catch (IOException io) {
//                    System.out.println("Ошибка чтения файла...");
//            } finally {
//                if (reader != null) {
//                    try {
//                        reader.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }

            // Java 7 -> try-with-resources
            try (BufferedReader reader = new BufferedReader(new FileReader(products))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException exc) {
                System.out.println("Ошибка чтения файла");
            }

        }

    }

}
