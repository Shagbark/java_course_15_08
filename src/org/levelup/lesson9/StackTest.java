package org.levelup.lesson9;

// IllegalArgumentException
public class StackTest {

    public static void main(String[] args) {
        Stack stack = new Stack(3);

        // try-catch-finally
        // try-catch
        // try-finally

        try {
            stack.push(54);
            stack.push(86);
            stack.push(200);
            stack.push(360);
        } catch (StackOverflowException exc) {
            exc.printStackTrace();
        } finally {
            System.out.println("Finally");
        }

        int result = stack.pop();
        System.out.println(result);
    }

}
