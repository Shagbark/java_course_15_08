package org.levelup.lesson1;

public class HelloWorld {

    public static void main(String[] args) {
        // Печать строки на экран
        System.out.println("Hello world!");

        int var;

        // Запись значения в переменную (в память)
        var = 634;

        System.out.println(var);

        int b = 85;
        int sum = var + b;

        System.out.println("String 1 plus " + "String 2");
        String s = "String1";
        String s2 = "String2";
        String s3 = s + s2;

        System.out.println("Sum: " + sum);
        System.out.println("String " + (var + b));

        // %
        int del = 7 % 3; // 15 % 4, 23 % 6
        System.out.println("Del: " + del);

        // ++, --
        int i = 10;
        i++; // i = 11
        i--; // i = 10

        // Префиксный инкремент: ++i;
        // Постфиксный инкремент: i++;


        int j = 15;

        // j++ + i-> 25; 1. i + j, 2. j++
        // i + ++j -> 27; 1. j = j + 1, 2. i + j

        System.out.println(j++); // 15
        System.out.println(++j); // 17

        // j = 15
        // System.out.println(j++)
        // 1. System.out.println(j)
        // 2. j = j + 1; -> 16

        // System.out.println(++j);
        // 1. j = j + 1; -> 17
        // 2. System.out.println(j)
    }

}
