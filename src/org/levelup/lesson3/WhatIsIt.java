package org.levelup.lesson3;

@SuppressWarnings("ALL")
public class WhatIsIt {

    public static void main(String[] args) {
        int value = 100;
        Account petrov = new Account("Petrov", 200); // amount = 200
        String s = "10";
        plus10(value);
        plus10InAccount(petrov);
        changeString(s);
        System.out.println(value);
        System.out.println(petrov.amount);
        System.out.println(s);
    }
    static void plus10(int value) {
        value = value + 10;
    }
    static void plus10InAccount(Account account) {
        account.amount = account.amount + 10;
    }
    static void changeString(String s) {
        s = s + "10";
        System.out.println(s);
    }

}
