package org.levelup.lesson3;

// 16B - 64
//

// 1 int - 24
// 1 double - 24
// 2 int - 24
// 3 int - 32

public class Account {

    // переменная класса, поле класса, field
    double amount;
    String fio; // null

    Account() {
        fio = "Anonymous";
    }

    Account(String newFio, double newAmount) {
        fio = newFio;
        amount = newAmount;
    }

    // increaseAndGet()
    // type name(parameters) {}

    // parameters - type nameOfParameter
    double increaseAndGet(double value) {
        if (value <= 0) {
            // return
            return amount;
        }
        // some code here
        amount += value; // amount = amount + value
        return amount;
    }

    // перегрузка - overloading
    double increaseAndGet(long value) {
        if (value <= 0) {
            // return
            return amount;
        }
        // some code here
        amount += value; // amount = amount + value
        return amount;
    }

    void changeFio(String newFio) {
        // return; Ничего не вернет, но завершит метод
        fio = newFio;
    }

}
