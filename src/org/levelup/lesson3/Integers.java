package org.levelup.lesson3;

public class Integers {

    public static void main(String[] args) {
        // int - Integer
        Integer i1 = 127;
        Integer i2 = 127;
        Integer i3 = 130;
        Integer i4 = 130;
        // Все упало
        // false false
        // false true
        // true false
        // true true
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);
    }

}
