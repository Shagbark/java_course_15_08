package org.levelup.lesson3;

import java.util.Locale;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Account anonymous = new Account();
        System.out.println("FIO: " + anonymous.fio);

        // int i = 10; ->  локальная переменная

        // petrovAccount - объект, экземпляр, ссылка, object, instance, reference
        Account petrovAccount = new Account("Petrov", 10);
//        petrovAccount.fio = "Petrov";
//        petrovAccount.amount = 10;

        Account ivanovAccount = new Account();
        ivanovAccount.fio = "Ivanov";
        ivanovAccount.amount = 15;

        System.out.println(petrovAccount.fio + " amount: " + petrovAccount.amount);
        System.out.println(ivanovAccount.fio + " amount: " + ivanovAccount.amount);

        // Пользователь вводит сумму, на которую надо уменьшить
        Scanner console = new Scanner(System.in);
        System.out.println("Введите сумму:");
        // 2.564 -> double
        double sum = console.nextDouble();

        double result = petrovAccount.increaseAndGet(sum);
        System.out.println("Petrov amount: " + result);

        petrovAccount.changeFio("Petrov Ivan");
        System.out.println("New fio: " + petrovAccount.fio);
        System.out.println(petrovAccount.fio + " amount: " + petrovAccount.amount);

        Account p1 = new Account("Petrov", 10);
        Account p2 = new Account("Petrov", 10);
        // Account p3 = p1;
        System.out.println(p1 == p2); // false
    }

}
